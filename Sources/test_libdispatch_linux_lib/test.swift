import Foundation



public final class MyOpSync : Operation {
	
	override public func main() {
		Thread.sleep(forTimeInterval: 1)
	}
	
}


public final class MyOpAsync : Operation {
	
	public enum State {
		
		case inited
		case running
		case finished
		
	}
	
	public let q = DispatchQueue(label: "Q", qos: .utility)
	
	override public func start() {
		state = .running
		q.async{
			Thread.sleep(forTimeInterval: 1)
			self.state = .finished
		}
	}
	
	private let stateSemaphore = DispatchSemaphore(value: 1)
	
	private var state = State.inited {
		willSet(newState) {
			willChangeValue(forKey: "state")
			
			if isAsynchronous {
				/* https://github.com/apple/swift-corelibs-foundation/blob/swift-4.1.2-RELEASE/Foundation/Operation.swift
				 * On Linux, the Foundation implementation differs from the close-
				 * source one we got on Apple’s platforms. Mainly, the original
				 * implementation expected changes to isExecuting and isFinished to
				 * be KVO-compliant and relied heavily on these notifications to
				 * act on the state of the operation.
				 * In pure Swift (no ObjC-runtime), KVO is not available (yet?). So
				 * a workaround has been implemented to mimic the old behaviour.
				 * However this (among other implementation details) results in a
				 * dispatch_leave_group method to be called twice (which triggers an
				 * assert and makes the program crash) for synchronous operations
				 * when we manually managed these properties.
				 * So for synchronous operations, the isExecuting and isFinished
				 * properties are managed by Operation itself. */
				let newStateExecuting = newState == .running
				let oldStateExecuting = state == .running
				let newStateFinished = newState == .finished
				let oldStateFinished = state == .finished
				
				if newStateExecuting != oldStateExecuting {willChangeValue(forKey: "isExecuting")}
				if newStateFinished  != oldStateFinished  {willChangeValue(forKey: "isFinished")}
			}
			
			stateSemaphore.wait()
		}
		didSet(oldState) {
			stateSemaphore.signal()
			
			if isAsynchronous {
				let newStateExecuting = state == .running
				let oldStateExecuting = oldState == .running
				let newStateFinished = state == .finished
				let oldStateFinished = oldState == .finished
				
				if newStateFinished  != oldStateFinished  {didChangeValue(forKey: "isFinished")}
				if newStateExecuting != oldStateExecuting {didChangeValue(forKey: "isExecuting")}
			}
			
			didChangeValue(forKey: "state")
		}
	}
	
	public override var isAsynchronous: Bool {
		return true
	}
	
	public final override var isExecuting: Bool {
		guard isAsynchronous else {return super.isExecuting}
		
		stateSemaphore.wait(); defer {stateSemaphore.signal()}
		return state == .running
	}
	
	public final override var isFinished: Bool {
		guard isAsynchronous else {return super.isFinished}
		
		stateSemaphore.wait(); defer {stateSemaphore.signal()}
		return state == .finished
	}
	
}

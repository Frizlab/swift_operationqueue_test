import XCTest
@testable import test_libdispatch_linux_lib



class Tests: XCTestCase {
	
	let operationQueue = OperationQueue()
	
	override func setUp() {
		super.setUp()

		operationQueue.maxConcurrentOperationCount = 1
	}
	
	override func tearDown() {
		operationQueue.cancelAllOperations()
		operationQueue.waitUntilAllOperationsAreFinished()

		super.tearDown()
	}
	
	func testSync() {
		let op = MyOpSync()
		
		operationQueue.addOperation(op)
		operationQueue.waitUntilAllOperationsAreFinished()
		XCTAssertTrue(op.isFinished)
	}
	
	func testAsync() {
		let op = MyOpAsync()
		
		operationQueue.addOperation(op)
		operationQueue.waitUntilAllOperationsAreFinished()
		XCTAssertTrue(op.isFinished)
	}
	
	func testAsync2() {
		let op = MyOpAsync()
		
		operationQueue.addOperation(op)
		op.waitUntilFinished()
		XCTAssertTrue(op.isFinished)
	}
	
	
	static var allTests = [
		("testSync", testSync),
		("testAsync", testAsync),
		("testAsync2", testAsync2)
	]
	
}
